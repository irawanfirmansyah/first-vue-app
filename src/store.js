import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';
// import { getAllTodo } from './APIutil';

const initialState = {
  todos: [],
}
const getTodos = async () => {
  await axios.get('http://localhost:3000/db')
    .then(response => {
      initialState.todos = response.data.todos
    })
}

getTodos();

Vue.use(Vuex)

const store = new Vuex.Store({
  state: initialState,
  mutations: {
    ADD_TODO(state, todo) {
      axios.post('http://localhost:3000/todos/', todo).then(() => {
        state.todos.push(todo);
      });
    },
    DELETE_TODO(state, todo) {
      axios.delete('http://localhost:3000/todos/' + todo.id).then(() => {
        state.todos = state.todos.filter((item) => todo.id !== item.id);
      });
    },
    EDIT_TODO(state, todo) {
      axios.put('http://localhost:3000/todos/' + todo.id, todo).then(() => {
        state.todos.map((item) => {
          if (item.id === todo.id) {
            item.description = todo.description;
            item.date = todo.date;
          }
        });
      });
    }
  },
  actions: {
    addTodo({ commit }, { todo }) {
      commit('ADD_TODO', todo);
    },
    deleteTodo({ commit }, { todo }) {
      commit('DELETE_TODO', todo);
    },
    editTodo({ commit }, { todo }) {
      commit('EDIT_TODO', todo);
    }
  },
  getters: {
    getTodoById: (state) => (id) => {
      var todo = state.todos.find((todo) => {
        todo.id === id
      })
      return todo
    },
    todoIsEmpty(state) {
      return state.todos.length === 0;
    }
  }
})

export default store;
