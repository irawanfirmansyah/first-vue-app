import axios from 'axios';

export const getAllTodo = async () => {
    let listOfTodo = [];
    await axios.get('http://localhost:3000/db')
    .then(response => {
        listOfTodo = response.data.todos
    })
    return listOfTodo;
}